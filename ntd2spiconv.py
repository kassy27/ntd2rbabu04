from pyjoyconforUbuntu import EnJoyCon
from develop_wii_board import library_wii_board
import pyRBAbu04
import time

JON_CONV_SPACE = 100
WII_CONV_INC = 455

if __name__ == '__main__':
    enjoy = EnJoyCon()
    wii_board wii_board_obj

    # device open
    enjoy.connect()
    pyRBAbu04.open()
    wii_board_obj.connect()


    while True:
        # initialize commands
        joycmd = (False,0.0)
        wii_cmd = (0,0)

        # com joycon
        if not enjoy.isConnect():
            enjoy.connect()
        else:
            joycmd = enjoy.getCommand()

        # com wii board
        if not wii_board_obj.isConnect():
            wii_board_obj.connect()
        else:
            wii_cmd = wii_board_obj.getCommand()


        # convert commands
        joy_a_value = joycmd[1]* (2048 - JON_CONV_SPACE)
        if joycmd[0]:
            joy_a_value += 2048 + JON_CONV_SPACE
        joy_a_value /=4095

        wii_a_value = wii_cmd[0] + wii_cmd[1]*3 + 0.5
        wii_a_value = wii_a_value * WII_CONV_INC / 4095

        # com RBAbu04
        pyRBAbu04.output(0, joy_a_value)
        pyRBAbu04.output(1, wii_a_value)

        time.sleep(0.1)


    # device close
    pyRBAbu04.close()

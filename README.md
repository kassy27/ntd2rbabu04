# ntd2RBAbu04

sample program to convert from Nintendo controllers signal to RBAbu04.

## Getting started

Clone under repos.

* [pyjoyconforUbuntu](https://gitlab.com/kassy27/pyjoyconforubuntu)
* [develop_wii_board](https://github.com/Takayanagi3104/develop_wii_board)
* [pyRBAbu04](https://github.com/DaiGuard/pyRBAbu04)

Execute.

```
python ntd2spiconv.py
```

## Project status
No hardware testing yet.

We will be testing soooon!!